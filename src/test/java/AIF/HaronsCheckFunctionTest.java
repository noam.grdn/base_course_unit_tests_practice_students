package AIF;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class HaronsCheckFunctionTest {
    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        aifUtil = new AIFUtil();
    }

    @BeforeClass
    public static void beforeAll(){
        aifUtil = new AIFUtil();//init  aerial vehicles and missions.
    }
    

    //קבוצות שקילות
    @Test//78
    public void testCheckHermesEquilibriumGroupsInGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),78);//84 - arbitrary
        assertTrue("failure - hoursOfFlightSinceLastRepair = 84 shouldn't be reset to 0 after Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 78);
    }


    @Test//175
    public void testCheckHaronEquilibriumGroupsAboveGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),175);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 0 should  0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

    //ערכי גבול

    @Test
    public void testCheckLimitsFighterJetsLowerBoundMiddle(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),0);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 0 should stay zero.", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsFighterJetsLowerBoundLeft(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),-1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -1 shouldn't be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }



    @Test
    public void testCheckLimitsFighterJetsLowerBoundRight(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 1 should not be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 1);
    }

    @Test
    public void testCheckLimitsFighterJetsUpperBoundLeft(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),149);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 149 shouldn't be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 149);
    }

    @Test
    public void testCheckLimitsFighterJetsUpperBoundMiddle(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),150);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 150 should be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsFighterJetsUpperBoundRight(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),151);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 151 should be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

}
