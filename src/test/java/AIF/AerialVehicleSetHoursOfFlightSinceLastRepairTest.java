package AIF;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class AerialVehicleSetHoursOfFlightSinceLastRepairTest {
    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        aifUtil = new AIFUtil();
    }

    @Test
    public void testLimitsLeft(){
        aifUtil.getAerialVehiclesHashMap().get("Shoval").setHoursOfFlightSinceLastRepair(-1);
        assertTrue("failure - hoursOfFlightSinceLastRepair should be fixed to zero", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testLimitsMiddle(){
        aifUtil.getAerialVehiclesHashMap().get("Shoval").setHoursOfFlightSinceLastRepair(0);
        assertTrue("failure - hoursOfFlightSinceLastRepair should stay zero", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testLimitsRight(){
        aifUtil.getAerialVehiclesHashMap().get("Shoval").setHoursOfFlightSinceLastRepair(1);
        assertTrue("failure - hoursOfFlightSinceLastRepair should stay one", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 1);
    }
}
